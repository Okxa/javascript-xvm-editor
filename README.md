# JavaScript XVM Editor

XVM: eXtended Visualization Mod is a mod for a game named World Of Tanks.

This is a web based editor which can load and save configuration files for that mod.

Visualizing changes as seen in the game was the goal of this project, however that was not implemented as of writing this README.
