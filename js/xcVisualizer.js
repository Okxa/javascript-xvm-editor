function drawXVMxc(object) {
    var keys = Object.keys(object);
    //type, id, htmlClass, innerHtml, parentElement, ahref, onClick, inputType,
    addElement("table", "authorInfoTable", "keyTable", null, "obj");
    addElement("tr", "authorInfoTableRow0", "keyTable", null, "authorInfoTable");
    addElement("th", null, "keyTable", "Key" ,"authorInfoTableRow0");
    addElement("th", null, "keyTable", "Value" ,"authorInfoTableRow0");

    addElement("tr", "authorInfoTableRow1", "keyTable", null, "authorInfoTable");
    addElement("th", null, "keyTable", "Author:" ,"authorInfoTableRow1");
    addElement("th", null, "keyTable", object.definition.author ,"authorInfoTableRow1");

    addElement("tr", "authorInfoTableRow2", "keyTable", null, "authorInfoTable");
    addElement("th", null, "keyTable", "Description:" ,"authorInfoTableRow2");
    addElement("th", null, "keyTable", object.definition.description ,"authorInfoTableRow2");

    addElement("tr", "authorInfoTableRow3", "keyTable", null, "authorInfoTable");
    addElement("th", null, "keyTable", "URL:" ,"authorInfoTableRow3");
    addElement("th", null, "keyTable", object.definition.url ,"authorInfoTableRow3");

    addElement("tr", "authorInfoTableRow4", "keyTable", null, "authorInfoTable");
    addElement("th", null, "keyTable", "Last Modified:" ,"authorInfoTableRow4");
    addElement("th", null, "keyTable", object.definition.date ,"authorInfoTableRow4");

    addElement("tr", "authorInfoTableRow5", "keyTable", null, "authorInfoTable");
    addElement("th", null, "keyTable", "Min. game version:" ,"authorInfoTableRow5");
    addElement("th", null, "keyTable", object.definition.gameVersion ,"authorInfoTableRow5");

    addElement("tr", "authorInfoTableRow6", "keyTable", null, "authorInfoTable");
    addElement("th", null, "keyTable", "Min. mod version:" ,"authorInfoTableRow6");
    addElement("th", null, "keyTable", object.definition.modMinVersion ,"authorInfoTableRow6");

    // config options
    addElement("table", "itemTable", "keyTable", null, "obj");
    addElement("tr", "itemTableRow0", "keyTable", null, "itemTable");
    addElement("th", null, "keyTable", "Key" ,"itemTableRow0");
    addElement("th", null, "keyTable", "Value" ,"itemTableRow0");

    addElement("tr", "itemTableRow1", "keyTable", null, "itemTable");
    addElement("th", null, "keyTable", "Config Version:" ,"itemTableRow1");
    addElement("th", null, "keyTable", object.configVersion ,"itemTableRow1");

    addElement("tr", "itemTableRow2", "keyTable", null, "itemTable");
    addElement("th", null, "keyTable", "Auto Reload config:" ,"itemTableRow2");
    addElement("th", null, "keyTable", object.autoReloadConfig ,"itemTableRow2");

    addElement("tr", "itemTableRow3", "keyTable", null, "itemTable");
    addElement("th", null, "keyTable", "Game Region:" ,"itemTableRow3");
    addElement("th", null, "keyTable", object.region ,"itemTableRow3");

    addElement("tr", "itemTableRow4", "keyTable", null, "itemTable");
    addElement("th", null, "keyTable", "Language:" ,"itemTableRow4");
    addElement("th", null, "keyTable", object.language ,"itemTableRow4");

}