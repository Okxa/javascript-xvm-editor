function hideContent(id){
    document.getElementById(id).style.display = "none";
}
function showContent(id){
    document.getElementById(id).style.display = "initial";
}

function editFile(filename) {
    var editableFiles = document.getElementsByClassName("editor");
    for (var i = 0; i < editableFiles.length; i++) {
        hideContent(editableFiles[i].id);
    }
    // show the editor
    showContent(filename + "editor");

    // visualize config
    var objekt = XCtoJSON(filename);
    visualizeXcSettingObject(filename, objekt)
}

// setting visualizer
function visualizeXcSettingObject(filename, object) {
    // element in which visualisation is created
    var visroot = document.getElementById("obj");
    // clear the previous
    visroot.innerHTML = "";
    // add title and refresh btn container
    addElement("div", "visTitleContainer", null, null, visroot.id );
    // add refresh button
    addElement("a", "refresh", null, "<img src='res/refresh.png' title='Refresh this view'>", "visTitleContainer", "#", "editFile(\"" + filename.toString() + "\")");
    // if filename is not null
    if (filename !== null) {
        // create title
        var title = document.createElement("h2");
        title.innerHTML = filename;
        document.getElementById("visTitleContainer").appendChild(title);
        // check for object
        if (object !== null) {
            // debug
            console.log(object);
            // check which file is selected
            if (filename === "@xvm.xc") {
                drawXVMxc(object);
            }
            else {
                objectToGui(object, "obj", filename, true);
            }
        }
        else {
            console.log("no object specified");
        }
    }
    else {
        console.log("no filename specified");
    }
}











// dumb object printer, doesnt work good if there are same name objects
function objectToGui(object, parentid, filename, isfresh) {
    if (isfresh === true) {
        document.getElementById(parentid).innerHTML = "";
    }
    var i = 0;
    for (var key in object) {
        if (typeof object[key] === "object" && object[key] != null) {
            // if key is object print and do again
            var keys = Object.keys(object);
            // debug
            //console.log(keys[i] + object[key]);

            //type, id, htmlClass, innerHtml, parentElement, ahref, onClick, inputType,
            addElement("div", keys[i] , "objectName" , ((object[key]) ? object[key].constructor.name : "null") + ": " + keys[i] , parentid, null, null);
            
            objectToGui(object[key], keys[i]);
        }
        // else just print the key...
        else {
            var keys = Object.keys(object);
            addElement("span", keys[i] , "key", keys[i] + ": " + ((object[key]) ? object[key] : "null") , parentid, null, null);
        }
        i++;
    }
}