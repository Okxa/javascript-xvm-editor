/**
 * Created by elgs on 4/18/16.
 */
;(function () {
    "use strict"

    var removeComments = function (x) {
        x = x.replace(/\\"|"(?:\\"|[^"])*"|(\/\*[\s\S]*?\*\/)/g, function (m, g1) {
            return !g1 ? m : ""; // if g1 is not set/matched, re-insert it, else remove
        });

        x = x.replace(/\\"|"(?:\\"|[^"])*"|(\/\/.*)/g, function (m, g1) {
            return !g1 ? m : ""; // if g1 is not set/matched, re-insert it, else remove
        });
        return x;
    };

    var removeTailingCommas = function (x) {
        x = x.replace(/\\"|"(?:\\"|[^"])*"|(,\s*(?=[\]}]))/g, function (m, g1) {
            return !g1 ? m : ""; // if g1 is not set/matched, re-insert it, else remove
        });
        return x;
    };

    var restoreKeyQuotes = function (x) {
        x = x.replace(/\\"|"(?:\\"|[^"])*"|([\w.]+?(?=:))/g, function (m, g1) {
            return !g1 ? m : '"' + m + '"'; // if g1 is not set/matched, re-insert it, else remove
        });
        return x;
    };

    var restoreMultilineText = function (x) {
        x = x.replace(/`[\s\S]*?`/g, function (m) {
            if (m === '``') {
                return '[]';
            }
            return JSON.stringify(m.slice(1, -1).split('\n'));
        });
        return x;
    };

    var toJSON = function (x) {
        x = removeComments(x);
        x = removeTailingCommas(x);
        x = restoreKeyQuotes(x);
        x = restoreMultilineText(x);
        return x;
    };

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        exports.toJSON = toJSON;
    } else {
        window.jsonx = {toJSON: toJSON};
    }

})();