var files = [];
var alreadyLoaded = false;

function saveFile(evt) {
    var fileName = evt.target.parentElement.id;
    var text = document.getElementById(fileName + "editor").value;
    // replace newline chars -> LF -> CR+LF
    text = text.replace(/\n/g, "\r\n");
    // when creating new file, add "\ufeff" == utf8 bom marker
    var newFile = new File(["\ufeff" + text], fileName, {type: "text/xc;charset=utf-8"});
    // save using
    // https://github.com/eligrey/FileSaver.js
    // file, name(gets it from file too), disable auto dom
    saveAs(newFile, null, true);
}
function saveAllFiles() {
    // get files contents
    var files = [];
    var filesEl = document.getElementsByClassName("editor");
    for (var i = 0; i < filesEl.length; i++) {
        // get filename from editor id
        let filename = filesEl[i].id.split("editor")[0];
        // get contents from textarea
        let text = filesEl[i].value
        // replace newline chars -> LF -> CR+LF
        text = text.replace(/\n/g, "\r\n");
        // when creating new file, add "\ufeff" == utf8 bom marker
        let file = new File(["\ufeff" + text], filename, { type: "text/xc;charset-utf-8"})
        files.push(file)
    }
    // create a zip
    var zip = new JSZip();
    // add all files
    for (var i = 0; i < files.length; i++) {
        zip.file(files[i].name, files[i]);
    }
    zip.generateAsync({type:"blob"})
    .then(function(content) {
        // see FileSaver.js
        saveAs(content, "config" + ".zip");
    });
}


// file loader thing
function loadFiles(evt) {
    if (alreadyLoaded === true) {
        reload();
    }
    alreadyLoaded = true;

    files = evt.target.files; // FileList object
    readFiles(files);
    fileSelect();
}
// load files over url
function loadFileFromURL(url) {
    // this uses the new'ish fecth api
    // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API

    // return here to allow return values
    return fetch(url)
      .then(res => res.blob()) // Gets the response and returns it as a blob
      .then(blob => {
        // Here's where you get access to the blob
        // And you can use it for whatever you want
        // Like calling ref().put(blob)

        // I turn it into a file to have the file have name...
        var file = new File([blob], url.substr(url.lastIndexOf('/') + 1));
        return file;
    });
}
function loadDefaultConfig() {

    if (alreadyLoaded === true) {
        reload();
    }
    alreadyLoaded = true;
    
    let reader = new FileReader();
    // here is how to use async
    // loadfilefromurl
    loadFileFromURL("xvmtemplate/filenames.txt").then(function(result) {
        // code here

        var nameFile = result 
        
        reader.readAsText(nameFile);
        reader.onload=function(){
            // split filename string to array
            var filenamearr = reader.result.split(/\r?\n/);
            //sort  alphabetically
            filenamearr.sort();
            // this is used to track when all files are loaded
            var iscompleted = 0;
            // iterate through and load the files
            for (var i = 0; i < filenamearr.length; i++) {
                // this is here so that i can acces the name in the next function
                let filename = filenamearr[i];
                loadFileFromURL("xvmtemplate/" + filename).then(function(resultb) {
                    iscompleted++;
                    // check if the file is unallowed
                    if (resultb.name === "filenames.txt") {
                        //console.log("removed file index txt");
                    }
                    else {
                        // add files to array
                        files.push(resultb);
                    }
                    // after all are added, do this
                    if (iscompleted === filenamearr.length) {
                        // read em
                        readFiles(files);
                        fileSelect();
                    }
                });
            }
        };

    });
}



// handles file menu
function fileSelect() {
    tablinks = document.getElementsByClassName("fileNameLink");
    // clear link style
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].classList.remove("active");
    }
    // if not event then select 1st file
    if (typeof(this.id) === "undefined") {
        document.getElementById("fileList").firstElementChild.className += " active";
        document.getElementById("editorTitle").innerHTML = document.getElementById("fileList").firstElementChild.id;
    }
    else {
        // get the real filename to be used..
        var realid = this.id.split("_");
        // ^ its sort of hack, because i changed the filelist.
        // set clicked link active style
        document.getElementById(realid[0]).className += " active";
        // set title
        document.getElementById("editorTitle").innerHTML = realid[0];
        // select for editing
        editFile(realid[0]);
    }

}

// ****************************************************************************************************************
// turn file object into text
function readFiles(filesList) {
    // adding links
    // **************************************
    for (var i = 0; i < filesList.length; i++) {
        //type, id, htmlClass, innerHtml, parentElement, ahref, onClick, inputType, multiselect
        addElement("div", filesList[i].name , "fileNameLink", null , "fileList", "#", null, null, null);
        addElement("a", filesList[i].name + "_link" , "FileNameLinkBtn", filesList[i].name , filesList[i].name, "#", null, null, null);
        // add save buttons
        document.getElementById(filesList[i].name).insertAdjacentHTML("afterbegin", "<img class='saveicon' src='res/save.png' title='Save this file...'>");
        
    }
    // adding eventListeners to links
    var fileLink = document.getElementsByClassName("FileNameLinkBtn");
    var saveLink = document.getElementsByClassName("saveicon");
    // first showing the file to edit.
    for (var i = 0; i < fileLink.length; i++) {
        fileLink[i].addEventListener('click', fileSelect, false);
    }
    // then for saving
    for (var i = 0; i < saveLink.length; i++) {
        saveLink[i].addEventListener('click', saveFile, false);
    }
    // ***************************************

    // adding editors
    // "let" instead of "var" to keep the vars in sync or whatever
    for (let i = 0; i < filesList.length; i++) {
        let reader = new FileReader();
        let name = filesList[i].name;
        reader.readAsText(filesList[i]);
        // create the editors automatically...
        reader.onload=function(){ createEditors(name, reader.result, i, filesList.length) };
        
    }
}


// ****************************************************************************************************************
// create editor fields
function createEditors(filename, text, runIndex, maxIndex) {
    // create the element
    var el = document.createElement("textarea");
    el.innerHTML = text;
    el.id = filename + "editor";
    el.className = "editor"
    el.style.display = "none";
    document.getElementById("fileEditor").appendChild(el);
    // do this only on last run
    if (runIndex === maxIndex - 1) {
        // enable saveall button
        document.getElementById("saveAll").style.display = "initial";
        // select first file as active editor
        editFile(document.getElementById("fileList").firstElementChild.id);
    }
}

function printFile(text, parent) {
    var el = document.createElement("PRE");
    // convert html special chars
    //el.innerText = el.textContent = text;
    //text = el.innerHTML;
    // ^^^^^^^^^^^^^^
    el.innerHTML = text;
    document.getElementById(parent).appendChild(el);
}


// event listener
document.getElementById('filesbtn').addEventListener('change', loadFiles, false);
