function XCtoJSON(filename) {

    var editableFiles = document.getElementsByClassName("editor");
    for (var i = 0; i < editableFiles.length; i++) {
        if (editableFiles[i].id === filename + "editor") {
            var jsonxString = editableFiles[i].value;
        }
    }

    // first convert JSONX to JSON
    var json = jsonx.toJSON(jsonxString);
    // and to object collection
    jsonObj = JSONXCtoObject(json).object;

    // returns object of the file contents
    return jsonObj;
}
function JSONXCtoObject(jsonString) {

    // Converts JSON file with xvm spec. referencers
    // to stardard JSON, to make parsing possible
    // ---------------------------------------------------------------------------------------
    // standard xvm .xc -config is JSONx, that is converted first to JSON.
    // however, the xvm syntax is not 100% jsonx, so it needs to be modified further
    // to allow using it in a object
    // ---------------------------------------------------------------------------------------
    // takes as parameter: JSON string, with xvm formatting
    // 
    //
    // returns standard json as string, 
    // and object which is collection of objects ie. the properties of the .xc config file.
    // ---------------------------------------------------------------------------------------

    
    var re = /\$\{\s*(".*":)?".*"\s*\}{1}/g;
    // ^matches these:
    // ${"alphaHP.very_low}
    // ${"alphaHP.very_low"     }
    // ${    "alphaHP.very_low"     }
    // ${"battleLabelsTemplates.xc":"def.hitlogHeader"}
    // ${"battleLabelsTemplates.xc":"def.hitlogHeader"    }
    // ${      "battleLabelsTemplates.xc":"def.hitlogHeader"}
    
    // check if references needs to be updated
    // by testing if the file has any strings matching to "re"
    if (re.test(jsonString)) {
        //console.log("First RegEx match");
        // reset index
        re.lastIndex = 0;
        // init necessary vars
        var item;
        // this array is used to modify the incorrect strings
        var extractedStrings = [];
        var output;


        // add every string that matches "re" to array:
        while (item = re.exec(jsonString)) {
            //console.log(item[0]);
            extractedStrings.push(item[0]);
        }

        var shortre = /\$\{\s*"[A-Za-z0-9_\.]*"\s*\}{1}/;
        // ^matches these:
        // ${"alphaHP.very_low}
        // ${"alphaHP.very_low"     }
        // ${    "alphaHP.very_low"     }
        var longre = /\$\{\s*"[A-Za-z0-9_\.]*":"[A-Za-z0-9_\.]*"\s*\}{1}/;
        // ^matches these:
        // ${"battleLabelsTemplates.xc":"def.hitlogHeader"}
        // ${"battleLabelsTemplates.xc":"def.hitlogHeader"    }
        // ${      "battleLabelsTemplates.xc":"def.hitlogHeader"}
        var propre = /"[A-Za-z_\.]*"/g;
        // ^matches these:
        // "asdjkasdjksad213123_...asd"
        // used later to detect property...

        var xcfile = "";
        var xcpath = "";

        // convert incorrect to json formatting

        /* turn all xvm file references like:
        *  
        *  ${"alphaHP.very_low"     }
        *  to:
        *  { "path" :"alphaHP.very_low" }
        *
        *  OR:
        *  ${ "battleLabelsTemplates.xc":"def.hitlogHeader" }
        *  to:
        *  { "file" : "battleLabelsTemplates.xc", "path": "def.hitlogHeader" }
        *
        *  ie. to json objects, to parse this all.
        */

        // run through all extracted "wrong format" strings
        for (var i = 0; i < extractedStrings.length; i++) {
            // if it has only one parameter
            if (shortre.test(extractedStrings[i])) {
                //console.log("Short RegEx match");
                // find the parameter
                xcpath = extractedStrings[i].match(propre);
                // update array with correctly formatted string
                extractedStrings[i] = "{ \"$ref\": { \"path\" :"  + xcpath + " } }";
                // update the original input string to have the right format as well
                jsonString = jsonString.replace(shortre, extractedStrings[i]);
            }
            // if it has two parameters
            else if (longre.test(extractedStrings[i])) {
                //console.log("Long RegEx match");
                // find the parameters
                xcfile = extractedStrings[i].match(propre)[0];
                xcpath = extractedStrings[i].match(propre)[1];
                // update array with correctly formatted string
                extractedStrings[i] = "{ \"$ref\": { \"file\" : " + xcfile + ", \"path\": " + xcpath + " } }";
                // update the original input string to have the right format as well
                jsonString = jsonString.replace(longre, extractedStrings[i]);
            }
        }
    }
    // parse to object from the JSON
    var configfile = JSON.parse(jsonString);

    // just for debug...
    // console.log(jsonString);
    // console.log(configfile);

    // return text and object which has array of objects,,,
    return  { text : jsonString, object : configfile };
}