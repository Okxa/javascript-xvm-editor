function include(script, onLoad) {
    var element = document.createElement("script");
    element.id = script;
    element.type = "text/javascript";
    element.onload = onLoad;
    element.src = "js/" + script + ".js";
    document.getElementsByTagName("head")[0].appendChild(element);
}
// element adder v1.0
function addElement(type, id, htmlClass, innerHtml, parentElement, ahref, clickAction, inputType, multi) {
    // check if type is null
    if (type != null) {
        domType = type;
    }
    else {
        // use span isntead if null
        domType = "span";
        var typeIsNull = true;
    }
    // create element
    var el = document.createElement(String(domType));
    // Setting ID
    // check if id is not null
    if (id != null) {
        el.id = String(id);
    }
    // Setting class
    // check if class is not null
    if (htmlClass != null) {
        el.classList.add(String(htmlClass));
    }
    // Setting innerHTML
    // check if innerHTML is not null
    if (innerHtml != null) {
        // check if type was null and
        // warn the user instead of
        // trying to use the users innerHTML
        if (typeIsNull != true) {
            el.innerHTML = String(innerHtml);
        }
        else {
            el.innerHTML = "type is null";
        }
    }
    else {
        if (typeIsNull != true) {
            el.innerHTML = "";
        }
        else {
            el.innerHTML = "type is null";
        }
    }
    //Setting link tags
    // check if DOM tagName is A
    if (el.tagName == "A") {
        // then check if ahref is not null
        if (ahref != null) {
            // and use ahref as href
            el.href = ahref;
        }
    }
    if (clickAction != null) {
        // add some onclick action
        console.log(clickAction);
        el.onclick = function() { eval(clickAction) };
    }
    // Setting input tags
    // check if DOM tagName is INPUT
    if (el.tagName == "INPUT") {
        // then check if id is not null
        if (id != null) {
            // if  multiple
            if (multi != null) {
                el.name = id + "[]";
            }
            else {
                el.name = id;
            }
        }
        // Setting form type
        // check if type is not null
        if (inputType != null) {
            el.type = inputType;
        }
        // Setting multiple type
        // check if type is not null
        if (multi != null) {
            el.multiple = true;
        }

    }
    // INSERTING ELEMENT
    // check if position is not null
    if (parentElement != null) {
        document.getElementById(String(parentElement)).appendChild(el);
    }
    else {
        var el = document.createElement("SPAN");
        el.innerHTML = "parentElement is null"
        document.body.appendChild(el);
    }
}
function reload() {
   window.location.reload(false);
}


// INCLUDES
// external libraries and like:
// https://github.com/eligrey/FileSaver.js
include("fileSaver");
// https://stuk.github.io/jszip/
include("jszip");
// https://github.com/elgs/JSONx-js
include("jsonx");

// program components:
include("fileHandler");
include("xcHandler");
include("guiHandler");
include("xcVisualizer");

//type, id, htmlClass, innerHtml, parentElement, ahref, onClick, inputType, multiple
// load defaul btn
addElement("a", "loadDefaults", "menubtn", "Load Default Config", "menu", "#", "loadDefaultConfig();");
// s-p-a-c-e-r
addElement("span", null, "menuspacer", "OR", "menu");
// loading files
addElement("span", "configtitle", "menubtn", "Select .xc Files:", "menu");
addElement("input", "filesbtn", null, null, "configtitle", null, null, "file", "y");
// save all
addElement("a", "saveAll", "menubtn", "<img class='saveicon' src='res/save.png'>Download All as .zip", "menu", "#", "saveAllFiles();");
// reset btn
addElement("a", "reset", "menubtn", "Reset Editor", "menu", "#", "reload();");